<h1>Project Summary:</h1>

- This is a restaurant page made with HTML, CSS & JS.
- The code is structured using Javascript modules & bundled with Webpack.
- If you disable Javascript, you will notice a blank page, this is because the entire pages are generated with Javascript.

<h2>Project Preview:</h2>

<img src="./assets/preview.png" alt="Project's preview">

<h3>How to run this project</h3>
<ul>
<li>`npx webpack --watch`</li>
</ul>

<h4>Asset's Authors:</h4>

- The following is a list under the form of "Image | Source | Author", of all the images sources and authors used in the project:

- Logo | Made it myself, using a Canva's template.
- Phone Icon | Bootstrap Icons.
- Envelope Icon | Bootstrap Icons.
- Every other image | Created with AI.
