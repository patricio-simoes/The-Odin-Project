import 'bootstrap-icons/font/bootstrap-icons.css';
import { image, paragraph } from "./index";
import "../CSS/about.css";
import mapImage from "../assets/images/map.jpg";

//? This script generates the content of the "About" section.

function createAbout(body) {
  const main = document.createElement("section");
  const about = document.createElement("article");
  const contact = document.createElement("article");
  const info = document.createElement("div");

  body.appendChild(main);
  main.appendChild(about);
  about.appendChild(image("map", mapImage, "Our location"));
  about.appendChild(contact);
  contact.appendChild(paragraph("Come visit us for an unforgettable dining experience or place an order online through our website. We look forward to serving you!"));
  contact.appendChild(info);
  info.appendChild(paragraph("(555) 123-4567", "<i class=\"bi bi-telephone-fill\"></i>"));
  info.appendChild(paragraph("contact@wasabiwonders.com", "<i class=\"bi bi-envelope-fill\"></i>"));
  info.appendChild(paragraph("123 Sushi Lane<br>Foodie City, FC 12345"));

  about.classList.add("about");
  contact.classList.add("contact");

  return main;
}

function loadAbout() {
    const main = document.querySelector("main");
    main.textContent = "";
    createAbout(main);
}

export default loadAbout;
