import { paragraph, element } from "./index";
import "../CSS/home.css";

//? This script generates the content of the "Home" section.

function createHome(body) {
  const main = document.createElement("section");
  const textArea = document.createElement("article");
  const asideArea = document.createElement("aside");

  textArea.classList.add("intro")
  asideArea.classList.add("footer-intro")

  body.appendChild(main);
  main.appendChild(element("div", "logo"));
  main.appendChild(textArea);
  main.appendChild(asideArea);

  textArea.appendChild(paragraph('"A Symphony of Sushi Delights"'));
  textArea.appendChild(paragraph('"Where Every Bite is a Wonder"'));
  asideArea.appendChild(paragraph("Dine In or Order Online Today!"));

  return main;
}

function loadHome() {
  const main = document.querySelector("main");
  main.textContent = "";
  createHome(main);
}

export default loadHome;
