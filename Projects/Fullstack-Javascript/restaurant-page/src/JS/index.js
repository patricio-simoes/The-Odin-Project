import '../CSS/style.css';
import '../CSS/reset.css';
import loadHome from './home.js'
import loadMenu from './menu.js'
import loadAbout from './about.js'

//? This script contains the common functions and elements used across the project.

createFramework();

bindEventListeners();

//* Creates the framework used across all three pages.
//* This framework is composed by the header and the footer.
function createFramework() {
  const header = document.querySelector("header");
  const footer = document.querySelector("footer");
  const footerText = document.createElement("div");
  const container = document.createElement("div");
  const title = document.createElement("h1");
  const navbar = document.createElement("nav");

  title.classList.add("title");
  title.innerHTML = "Wasabi Wonders";

  header.appendChild(container);
  container.appendChild(title);
  container.appendChild(navbar);
  navbar.appendChild(button("Home"));
  navbar.appendChild(button("Menu"));
  navbar.appendChild(button("About"));
  footer.appendChild(footerText);
  footerText.appendChild(paragraph("Made by Patrício Simões"));
  footerText.appendChild(anchor("&#8617; Back to the Projects Hub", "../index.html"));

  //* Loads the home section by default.
  loadHome();
  setActiveButton(document.querySelector(".navbar-btn:first-child"));

}

//* Creates a navbar button and binds the class "nav-btn" to it.
function button(name) {
  const btn = document.createElement("button");
  btn.innerHTML = name;
  btn.classList.add("navbar-btn");
  return btn;
}

//* Binds a click event to all buttons and calls the respective functions 
//* depending on the one that was clicked.
function bindEventListeners() {
  const sections = document.querySelectorAll('.navbar-btn');
  sections.forEach((button, index) => {
    button.addEventListener('click', (e) => {
      if (e.target.classList.contains('active'))
        return;
  
      switch(index) {
        case 0:
          loadHome();
          break;
        case 1:
          loadMenu();
          break;
        case 2:
          loadAbout();
          break;
        default:
          break;
      }
  
      resetActiveButtons(sections);
      setActiveButton(e.target);
    });
  });
}

//* Sets the active button. (Home, Menu or About).
function setActiveButton(btn) {
  btn.classList.add('active');
}

//* Resets all buttons. This is necessary for a smooth navigation experience.
function resetActiveButtons(list) {
  list.forEach(button => button.classList.remove('active'));
}

//* Creates and returns a paragraph element with or without an icon attached.
function paragraph(string, icon) {
  const p = document.createElement("p");
  if (icon == null)
    p.innerHTML = string;
  else
    p.innerHTML = icon + string;
  return p;
}

//* Creates and returns an element and binds a class to it.
function element(type, identifier) {
  const element = document.createElement(type);
  element.classList.add(identifier);
  return element;
}

//* Creates and returns an image and binds a class to it.
function image(identifier, src, alt) {
  const img = document.createElement("img");
  img.src = src;
  img.alt = alt;
  img.classList.add(identifier);
  return img;
}

function anchor(text, link) {
  const a = document.createElement("a");
  a.href = link;
  a.innerHTML = text;
  a.target = "_blank";
  return a;
}

export { paragraph, element, image }