import { paragraph } from "./index";
import "../CSS/menu.css";
import soupImage from "../assets/images/soup.jpg";
import californiaRollImage from "../assets/images/californiaRoll.jpg";
import spicyTunaRollImage from "../assets/images/spicyTunaRoll.jpg";
import salmonSashimiImage from "../assets/images/salmonSashimi.jpg";

//? This script generates the content of the "Menu" section.

function createMenu(body) {
  const main = document.createElement("section");
  const grid = document.createElement("div");
  grid.classList.add("menu");

  grid.appendChild(
    createGridItem(
      soupImage,
      "Miso Soup",
      "Miso Soup",
      "Traditional Japanese soup with tofu, seaweed, and green onions.",
      "$4.00"
    )
  );
  grid.appendChild(
    createGridItem(
      californiaRollImage,
      "California Roll",
      "California Roll",
      "Crab meat, avocado, and cucumber wrapped in seaweed and rice..",
      "$10.00"
    )
  );
  grid.appendChild(
    createGridItem(
      spicyTunaRollImage,
      "Spicy Tuna Roll",
      "Spicy Tuna Roll",
      "Fresh tuna, spicy mayo, and cucumber wrapped in seaweed and rice.",
      "$12.00"
    )
  );
  grid.appendChild(
    createGridItem(
      salmonSashimiImage,
      "Salmon Sashimi",
      "Salmon Sashimi",
      "Thinly sliced fresh salmon served with soy sauce and wasabi..",
      "$18.00"
    )
  );

  body.appendChild(main);
  main.appendChild(grid);

  return main;
}

//* Creates a grid item, aka, a sushi menu entry.
function createGridItem(src, alt, title, description, price) {
  const gridItem = document.createElement("article");
  gridItem.classList.add("menu-item");
  const img = document.createElement("img");
  const textArea = document.createElement("div");
  img.src = src;
  img.alt = alt;
  textArea.appendChild(paragraph(title));
  textArea.appendChild(paragraph(description));
  textArea.appendChild(paragraph(price));
  gridItem.appendChild(img);
  gridItem.appendChild(textArea);
  return gridItem;
}

function loadMenu() {
  const main = document.querySelector("main");
  main.textContent = "";
  createMenu(main);
}

export default loadMenu;
