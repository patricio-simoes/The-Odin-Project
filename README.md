<p>This is a repository created to hold and control every project i made while taking <a href="https://www.theodinproject.com/" target="_blank">The Odin Project</a>, a FOSS full stack curriculum on web development.</p>
<p>The Odin Project's curriculum's completion is currently a side hobby of mine that i like to explore during my free time.</p>
<p><b>A quick note:</b>
All of my projects are designed to go as low as the Iphone's SE screen size, (375px), they are in no way optimized for screens bellow that.</p>
